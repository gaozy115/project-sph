import requests from './request'
import mockRequests from './mockAjax'

export const reqCategoryList = () =>
  // 发请求 axios发请求返回结果对象（箭头函数简写了）
  requests(
    {

      url: '/product/getBaseCategoryList',
      method: 'get'
    })
export const reqGetBannerList = () => mockRequests.get('/banner')
// console.log(reqCategoryList())

