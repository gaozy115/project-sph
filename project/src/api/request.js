import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
const requests = axios.create({
    baseURL: '/api', /* 基于什么路径 */
    timeout:5000
})

// 请求拦截器，在请求发出去之前做一些事情

requests.interceptors.request.use(
   (config)=>{
    nprogress.start();
    return config;
  },
  
)

// 相应拦截器
requests.interceptors.response.use((res)=>
{
  nprogress.done();
  return res.data
},(error)=>{ 
  return Promise.reject(error);
})
export default requests


