import Vue from 'vue'

import App from './App.vue'
import router from '@/router'
import store from '@/store'
import TypeNav from '@/components/TypeNav'
// import {reqCategoryList} from '@/api'
// reqCategoryList()
Vue.component('TypeNav', TypeNav)
Vue.config.productionTip = false
import '@/mock/mockServe'
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
