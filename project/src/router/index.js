// 配置路由
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// 引入路由组件
import Home from '@/pages/Home'
import search from '@/pages/search'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
let originPush = VueRouter.prototype.push
let originReplace = VueRouter.prototype.replace
VueRouter.prototype.push = function (location, resolve, reject) {
  if (resolve || reject) {
    originPush.call(this, location, resolve, reject)
  } else {
    originPush.call(this, location, () => { }, () => { })
  }
}

VueRouter.prototype.replace = function (location, resolve, reject) {
  if (resolve || reject) {
    originReplace.call(this, location, resolve, reject)
  } else {
    originReplace.call(this, location, () => { }, () => { })
  }
}

export default new VueRouter({
  // 配置路由
  routes: [
    {
      // 用户第一次访问网站页面("/根目录"首页)时，地址栏里边没有“#锚点”的信息，
      // 也就没有对应的组件用于显示，用户体验不好，现在可以通过重定向实现一种效果，即当浏览器没有任何 #锚点信息时，默认也给显示一个组件。
      path: '/',
      redirect: '/home',   // 重定向地址 
    },
    {
      path: '/home',
      component: Home,
      meta: { show: true }
    },
    {
      path: '/search/:keyword?',
      component: search,
      meta: { show: true },//路由元信息配置，配合前面，看是否显示footer
      name: 'search'
    },
    {
      path: '/Login',
      component: Login,
      meta: { show: false }
    },
    {
      path: '/Register',
      component: Register,
      meta: { show: false }
    },
  ]
})